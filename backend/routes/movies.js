const { response, request } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()


router.get('/abc',(request,response)=>{
    response.send('only for check........')
})

//movie_id int primary key auto_increment, movie_title varchar (200),
//movie_release_date date , movie_time TIME, director_name varchar (100)
    
router.get('/',(request,response)=>{
    const statement = 'select * from movie'
  
  db.execute(statement,(error,result)=>{
    response.send(utils.createResult(error,result));
  })
  })
  
  
  router.post('/', (request, response) => {
    const { movie_title, director_name } = request.body
  
    const statement = `
      INSERT INTO movie
        (movie_title, director_name )
      VALUES
        ('${movie_title}','${director_name}')
    `
    db.execute(statement, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  
  router.put('/:movie_id', (request, response) => {
    const { movie_id } = request.params
    const { director_name } = request.body
  
    const statement = `
      UPDATE movie
      SET
      director_name = '${director_name}'
        
      WHERE
      movie_id = ${movie_id}
    `
    db.execute(statement, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  
  router.delete('/:movie_id', (request, response) => {
    const { movie_id } = request.params
  
    const statement = `
      DELETE FROM movie
      WHERE
      movie_id = ${movie_id}
    `
    db.execute(statement, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  
  



module.exports= router