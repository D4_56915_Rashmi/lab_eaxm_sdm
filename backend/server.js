const express = require('express')
const movie = require('./routes/movie')
const app = express()

app.use(express.json())
app.use('/movie',movie)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port 4000')
})
